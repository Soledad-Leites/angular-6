import { ObjectUnsubscribedError } from 'rxjs';

export class DestinoViaje {
    
    //selected: boolean;
    //public get selected(): boolean {
     //   return this._selected;}

    //public set selected(value: boolean) {
     //   this._selected = value; }

    public selected:boolean;

    public servicios: string[];


    constructor(public nombre:string, public u:string, public votes: number = 0) {
        this.servicios = ['desayuno', 'spa'];
     }
    isSelected():boolean {
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }
}
