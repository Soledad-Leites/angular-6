import {  DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';
import { Injectable } from '@angular/core';


@Injectable()
export class DestinosApiClient {
    //destinos: DestinoViaje[];
    //current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor(private store: Store<AppState>){
    }

    add(d:DestinoViaje){
        this.store.dispatch(new NuevoDestinoAction(d));
    }

    /*getById(id: String): DestinoViaje{
        return this.destinos.filter(function(d){return d.id.toString() === id;}) [0];
    }
    Dice que id no esta declarada en destino-viaje.model.ts ++++ No se que pasa ++++
    */

    elegir(d: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAction(d));

        //this.destinos.forEach(x => x.setSelected(false));
        //d.setSelected(true);
        //this.current.next(d);
    }

}